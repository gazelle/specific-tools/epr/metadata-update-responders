/// IMPORT
//////////////
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import groovy.sql.Sql;
import javax.sql.rowset.serial.*
import groovy.util.Node
import groovy.xml.*
import com.eviware.soapui.support.XmlHolder
import groovy.io.FileType

try {

    /// Generate new uuid
    //////////////////////////////////////////////////////////////////////////////////
    context.mockService.setPropertyValue('uuid1', 'urn:uuid:' + UUID.randomUUID())
    context.mockService.setPropertyValue('uuid2', 'urn:uuid:' + UUID.randomUUID())

    /// def for get holder node, def for psql Driver, def for call external testSuite
    //////////////////////////////////////////////////////////////////////////////////
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/update_metadata", "gazelle", "gazelle", "org.postgresql.Driver")

    // Namespace declaration
    holder.namespaces['lcm'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0'
    holder.namespaces['rim'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0'
    holder.namespaces['rs'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0'
    holder.namespaces['xdsb'] = 'urn:ihe:iti:xds-b:2007'
    holder.namespaces['xop'] = 'http://www.w3.org/2004/08/xop/include'
    holder.namespaces['wsse'] = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'
    holder.namespaces['saml2'] = 'urn:oasis:names:tc:SAML:2.0:assertion'
    holder.namespaces['xs'] = 'http://www.w3.org/2001/XMLSchema'
    holder.namespaces['ds'] = 'http://www.w3.org/2000/09/xmldsig#'
    holder.namespaces['soap'] = 'http://www.w3.org/2003/05/soap-envelope'
    holder.namespaces['wsa'] = 'http://www.w3.org/2005/08/addressing'

    /////// DEF XPATH for retrieve values from the soap Request
    //////////////////////
    //// def XPath getDomNode
    def assertion = holder.getDomNode("//soap:Header/wsse:Security/saml2:Assertion").toString()
    def document = new XmlHolder(holder.getDomNode("//lcm:SubmitObjectsRequest")).getXml()

    //// def XPath holder value
    def assertionRole = holder["//saml2:Assertion/saml2:AttributeStatement/saml2:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:subject:role']/saml2:AttributeValue/Role/@code"]
    def assertionNameId = holder["//saml2:Assertion/saml2:Subject/saml2:NameID"]
    def classificationScheme = holder["//rim:ExtrinsicObject/rim:Classification/@classificationScheme"]
    def version = holder["//rim:Association/rim:Slot[@name='PreviousVersion']/rim:ValueList/rim:Value"]

//====== xPath for retrieve value in the XML file =======

    def submissionSetSourceId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSSubmissionSet.sourceId\']]/@value"]
    def submissionSetUniqueId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSSubmissionSet.uniqueId\']]/@value"]
    def submissionSetPatientId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSSubmissionSet.patientId\']]/@value"]

    def documentEntryUniqueId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSDocumentEntry.uniqueId\']]/@value"]
    def documentEntryPatientId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSDocumentEntry.patientId\']]/@value"]
    def homeId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/@home"]
    def sourcePatientId = holder["//rim:Slot[@name=\'sourcePatientId\']/rim:ValueList/rim:Value"]


    if (!assertionValidation(assertion)) {
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Error_Response'
    }
    /*
    if(!requestValidation(document)){
        return "Error_Response"
    }
    */

    if (!patientIdExist(sql, documentEntryPatientId, sourcePatientId)) {
        errorCode = "XDSPatientIDReconciliationError"
        codeContext = "Update encountered error where Patient IDs did not match"
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />')
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Error_Response'
    }

    if (!idSourceExist(sql, submissionSetSourceId)) {
        errorCode = "XDSMetadataUpdateError"
        codeContext = "General metadata update error (An error occurred while checking the source_id)"
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />')
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Error_Response'
    }

    if (!uniqueIdExist(sql, documentEntryUniqueId)) {
        errorCode = "XDSMetadataUpdateError"
        codeContext = "General metadata update error (An error occurred while checking the unique_id)"
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />')
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Error_Response'
    }

    if (!statusIsApproved(sql, documentEntryUniqueId)) {
        errorCode = "XDSMetadataUpdateError"
        codeContext = "General metadata update error (An error occurred while checking availability status)"
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />')
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Error_Response'
    }

    if (!versionIsLast(sql, documentEntryUniqueId, version)) {
        errorCode = "XDSMetadataVersionError"
        codeContext = "The version number included in the update request did not match the existing object. One cause of this is multiple simultaneous update attempts"
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />')
        return 'Error_Response'
    }

    def messageDoc = getMessage(sql, documentEntryUniqueId)
    int nbrMetadataUpdated = countUpdate(sql, classificationScheme, messageDoc, holder).toInteger()

    if (nbrMetadataUpdated >= 1) {

        messageUpdated = getDocumentUpdate(sql, documentEntryUniqueId, message, document, nbrMetadataUpdated)
        int version_doc = getlastVersion(sql, documentEntryUniqueId)
        updateStatus(sql, documentEntryUniqueId)
        String patientId = getIdPatient(sql, documentEntryUniqueId)
        byte[] messageBase64 = Base64.encodeBase64(messageUpdated.getBytes());
        String messageToVal = new String(messageBase64)
        insertInRegistry(sql, patientId, documentEntryUniqueId, submissionSetSourceId, version_doc, homeId, submissionSetUniqueId, messageToVal)
    }

    context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
    return 'Success_Response'

} catch (Exception e) {
    context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('soapFaultReason', 'An error occurred while updating metadata')
    return 'mock_serverFault_generalResponse'

}

///////////////////////////////////////////////
//////////    List Function
///////////////////////////////////////////////
/*
- getDocumentUpdate
- countUpdate
- assertionValidation
- requestValidation
- patientIdExist
- idSourceExist
- uniqueIdExist
- statusIsApproved
- versionIsLast
- getIdPatient
- getMessage
- getPublishBy
- getResourceId
- getlastVersion
- updateStatus
- updateDocumentMessage
- insertInRegistry
*/
///
//////////////////////////////////////////////

//// Return document update or no
///////////////////////////
def getDocumentUpdate(def sql, def getUniqueId, def document_database, def document_request, def count) {

    def doc_o = new String(document_database.decodeBase64())

    if (count >= 1) {
        doc_o = doc_o.replace('StatusType:Approved', 'StatusType:Deprecated')
        byte[] messageBase64 = Base64.encodeBase64(doc_o.getBytes());
        String messageToVal = new String(messageBase64)
        updateDocumentMessage(sql, messageToVal, getUniqueId)
        return document_request;
    } else {
        return doc_o;
    }
}

//// count update metadata in the request
///////////////////////////
int countUpdate(def sql, def classification, def document_database, def holder) {

    def doc_o = new String(document_database.decodeBase64())
    int nbrUpdate = 0;

    for (arg in classification) {

        if (!doc_o.contains(arg) || !authorizationRole(sql, arg, "PAT", holder)) {
            break;
        }
        /// retrieve database document
        getpartDocEntry = doc_o.substring(0, doc_o.indexOf(arg))
        getpartDocEntry = doc_o.replace(getpartDocEntry, '')
        getClassi = getpartDocEntry.substring(0, getpartDocEntry.indexOf('</rim:Classification>'))

        /// retrieve request metadata
        nodeRep = holder['//rim:ExtrinsicObject//rim:Classification[@classificationScheme="' + arg + '"]/@nodeRepresentation']
        nodeRep = nodeRep.toString()
        nodeRep = nodeRep.replace('[]', '')
        representation = 'nodeRepresentation="' + nodeRep + '"'

        log.info nodeRep + ' <=== Node'

        if (!nodeRep.isEmpty()) {
            if (!getClassi.contains(representation)) {
                nbrUpdate++
            }
        }
    }

    log.info "Metadata Updated :" + nbrUpdate

    return nbrUpdate;
}

//// Check assertion SAML validaton
//////////////////////////////
def assertionValidation(def assertion) {
    log.info "Assertion SAML :"
    def project = mockRequest.getContext().getMockService().getProject()
    def testCase = project.testSuites['Library'].testCases['assertionValidation TestCase']

    if (assertion == 'null') {
        errorCode = "SAMLAssertionError"
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="Missing SAML Assertion" />')
        return false;
    }
    assertion = assertion.replace('<?xml version="1.0" encoding="UTF-8"?>', '')
    testCase.setPropertyValue('assertionDocument', assertion)
    def testCaseContext = new com.eviware.soapui.support.types.StringToObjectMap();
    def runner = testCase.run(testCaseContext, false)
    def getValidationAssertion = testCase.getPropertyValue("validationResponse")
    def getErrorMessage = testCase.getPropertyValue("errorMessage")

    if (!getValidationAssertion.contains('<ValidationTestResult>PASSED</ValidationTestResult>')) {
        context.mockService.setPropertyValue('soapError', getErrorMessage)
        return false;
    }
    return true;
}

//// Validation Message request ITI-57
/////////////////////////////////////
def requestValidation(def document) {

    log.info "Request :"
    def project = mockRequest.getContext().getMockService().getProject()
    def testCase2 = project.testSuites['Library'].testCases['updateMetadataValidation TestCase']

    testCase2.setPropertyValue('updateMetadataDocument', document)
    def testCaseContext = new com.eviware.soapui.support.types.StringToObjectMap();
    def runner = testCase2.run(testCaseContext, false)
    def getValidationAssertion = testCase2.getPropertyValue("validationResponse")
    def getErrorMessage = testCase2.getPropertyValue("errorMessage")

    if (!getValidationAssertion.contains('<ValidationTestResult>PASSED</ValidationTestResult>')) {
        context.mockService.setPropertyValue('soapError', getErrorMessage)
        return false;
    }
    return true;
}

//// Check if the Patient ID exist in the registry
////////////////////////////
def patientIdExist(def sql, def getPatientId, def sourcePatientId) {
    //// Get patient_id in Repository DB
    if(getPatientId!=sourcePatientId){
        return false;
    }
    request = sql.eachRow("select count(*) as nbrPatientId from document_registry where patient_id like ? ;", [getPatientId])
            { p -> patientId_doc = "${p.nbrPatientId}" }
    patientId_doc = Integer.parseInt(patientId_doc)

    if (patientId_doc >= 1) {
        return true;
    } else {
        return false;
    }

}

//// Check if the ID Source exist in the registry
////////////////////////////
def idSourceExist(def sql, def getIdSource) {
    //// Get source_id in Repository DB
    request = sql.eachRow("select count(*) as nbrSourceId from document_registry where sub_source_id like ? ;", [getIdSource])
            { p -> sourceId_doc = "${p.nbrSourceId}" }
    sourceId_doc = Integer.parseInt(sourceId_doc)

    if (sourceId_doc >= 1) {
        return true;
    } else {
        return false;
    }

}

//// Check if the Unique ID exist in the registry
////////////////////////////
def uniqueIdExist(def sql, def getUniqueId) {
    //// Get unique_id in Repository DB
    request = sql.eachRow("select count(*) as nbrUniqueId from document_registry where doc_unique_id like ? ;", [getUniqueId])
            { p -> uniqueId_doc = "${p.nbrUniqueId}" }
    uniqueId_doc = Integer.parseInt(uniqueId_doc)

    if (uniqueId_doc >= 1) {
        return true;
    } else {
        return false;
    }

}

//// Check if the statusIsApproved in the registry
////////////////////////////
def statusIsApproved(def sql, def getUniqueId) {

    ///// Get lastVersion_doc in DB
    request = sql.eachRow("select count(*) as nbrlastVersion from document_registry where doc_unique_id like ? and availability_status like 'Approved';", [getUniqueId])
            { p -> nbrlastVersion = "${p.nbrlastVersion}" }
    nbrlastVersion = Integer.parseInt(nbrlastVersion)


    if (nbrlastVersion >= 1) {
        return true;
    } else {
        return false;
    }
}

//// Check if the version in the registry
////////////////////////////
def versionIsLast(def sql, def getUniqueId, def getVersion) {
    ///// Get version in DB
    request = sql.eachRow("select version from document_registry where doc_unique_id like ? and availability_status like 'Approved';", [getUniqueId])
            { p -> version_doc = "${p.version}" }
    version_doc = Integer.parseInt(version_doc)

    if (getVersion <= version_doc) {
        return false;
    } else {
        return true;
    }
}

//// get patient_id in registry
///////////////////////////
def getIdPatient(def sql, def getUniqueId) {

    request = sql.eachRow("select patient_id from document_registry where doc_unique_id like ?;", [getUniqueId])
            { p -> id_patient = "${p.patient_id}" }

    return id_patient;
}

//// get message in registry
///////////////////////////
def getMessage(def sql, def getUniqueId) {
    request = sql.eachRow("select document from document_registry where doc_unique_id like ? and availability_status like 'Approved';", [getUniqueId])
            { p -> message = "${p.document}" }
    return message;
}

//// get resource in repo
///////////////////////////
def getResourceId(def sql, def getUniqueId) {
    request = sql.eachRow("select patient_id from document_registry where doc_unique_id like ?", [getUniqueId])
            { p -> resource_id_database = "${p.patient_id}" }
    resource_id_database = resource_id_database.substring(0, resource_id_database.indexOf("^"))
    return resource_id_database;
}

//// get the last version in repo
///////////////////////////
def getlastVersion(def sql, def getUniqueId) {

    request = sql.eachRow("select version from document_registry where doc_unique_id like ? and availability_status like 'Approved';", [getUniqueId])
            { p -> version_doc = "${p.version}" }
    version_doc = Integer.parseInt(version_doc)
    version_doc = version_doc + 1
    return version_doc;
}

//// Update the status in repo
///////////////////////////
def updateStatus(def sql, def getUniqueId) {
    request = sql.execute("Update document_registry set availability_status = 'Deprecated' where doc_unique_id like ? ;", [getUniqueId]);
}

//// Update the document message in repo
///////////////////////////
def updateDocumentMessage(def sql, def messageVal, def getUniqueId) {
    request = sql.execute("Update document_registry set document = ? where doc_unique_id like ? ;", [messageVal, getUniqueId]);
}

//// Insert the new document in repo
///////////////////////////
def insertInRegistry(
        def sql,
        def id_pat,
        def getUniqueId,
        def getIdSource,
        def version,
        def homeCommunity,
        def subUniqueId,
        def messageToVal) {

    def SQL = "INSERT INTO document_registry (patient_id, doc_unique_id, sub_source_id, version, availability_status, home_community_id, document, sub_unique_id) VALUES ( ?, ?, ?, ?, 'Approved', ?, ?, ?) ;"
    request = sql.execute(SQL, [id_pat, getUniqueId, getIdSource, version, homeCommunity, messageToVal, subUniqueId])

}

//// Check if the subject role is allowed to update the metadata
//////////////////////////
def authorizationRole(def sql, def arg, def initiator_role, def holder) {

    def getUniqueId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSDocumentEntry.uniqueId\']]/@value"]
    def resource_id_database = getResourceId(sql, getUniqueId)
    def role = holder["//*:Assertion/*:AttributeStatement/*:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/*:Role/@code"]
    def resourceId = holder["//*:Assertion//*:Subject//*:NameID"]

    if (initiator_role == "HCP" && role == "PAT") {
        if (role == "PAT" && arg == "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f") {
            return true;
        } else {
            context.mockService.setPropertyValue('soapInfo', '<detail>The confidentialityCode and deletionStatus that can always be changed by patients but never by healthcare professionals</detail>')
            return false;
        }
    } else if (initiator_role == "PAT" && role == "HCP") {
        context.mockService.setPropertyValue('soapInfo', '<detail>Metadata of documents published by patients must not be updated by health care professionals.</detail>')
        return false;

    } else if (initiator_role == "HCP" && role == "HCP") {
        if (resourceId != resource_id_database) {

            context.mockService.setPropertyValue('soapInfo', '<detail>Metadata of documents published by health care professionals are allowed to be updated by other health care professionals, but only in the own community, with the exception of the availabilityStatus which can also be changed cross community.</detail>')
            return false;
        }
    } else if (role == "DADM") {
        return true;
    } else {
        return true;
    }
}

