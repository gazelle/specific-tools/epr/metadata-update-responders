/// IMPORT
//////////////
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import groovy.sql.Sql;
import javax.sql.rowset.serial.*
import groovy.util.Node
import groovy.xml.*
import com.eviware.soapui.support.XmlHolder
import groovy.io.FileType

try {

// create XmlHolder for request content

    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def project = mockRequest.getContext().getMockService().getProject()
    def testCase = project.testSuites["Library"].testCases["ITI-57"]
    def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/update_metadata", "gazelle", "gazelle", "org.postgresql.Driver")

    // Namespace declaration
    holder.namespaces['lcm'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0'
    holder.namespaces['rim'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0'
    holder.namespaces['rs'] = 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0'
    holder.namespaces['xdsb'] = 'urn:ihe:iti:xds-b:2007'
    holder.namespaces['xop'] = 'http://www.w3.org/2004/08/xop/include'
    holder.namespaces['wsse'] = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'
    holder.namespaces['saml2'] = 'urn:oasis:names:tc:SAML:2.0:assertion'
    holder.namespaces['xs'] = 'http://www.w3.org/2001/XMLSchema'
    holder.namespaces['ds'] = 'http://www.w3.org/2000/09/xmldsig#'
    holder.namespaces['soap'] = 'http://www.w3.org/2003/05/soap-envelope'
    holder.namespaces['wsa'] = 'http://www.w3.org/2005/08/addressing'

//====== xPath for retrieve value in the XML file =======

    def submissionSetPatientId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSSubmissionSet.patientId\']]/@value"]
    def documentEntryUniqueId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSDocumentEntry.uniqueId\']]/@value"]
    def documentEntryPatientId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/rim:ExternalIdentifier[rim:Name/rim:LocalizedString[@value=\'XDSDocumentEntry.patientId\']]/@value"]
    def homeId = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryPackage/@home"]
    def homeIdDocument = holder["//lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:ExtrinsicObject/@home"]
    def sourcePatientId = holder["//rim:Slot[@name=\'sourcePatientId\']/rim:ValueList/rim:Value"]

    def assertion = new XmlHolder(holder.getDomNode("//wsse:Security/saml2:Assertion")).getXml()

    if (!assertionValidation(assertion)) {
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString());
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString());
        return 'Error_Response'
    }

    if (checkCommunity(sql, homeId, documentEntryPatientId) || homeId != homeIdDocument) {
        errorCode = "XDSUnknownCommunity"
        codeContext = "A value for the homeCommunityId is not recognize (" + homeId + ")"
        context.mockService.setPropertyValue('soapError','<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />');
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString());
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString());
        return "Error_response"
    }

/// Condition for valid request
///////////////////////////////

    if (submissionSetPatientId.toString().equals(documentEntryPatientId.toString()) && sourcePatientId.equals(documentEntryPatientId.toString())) {

        // transfer to iti-57 simu
        //////////////////////////
        def registryObjectList = new com.eviware.soapui.support.XmlHolder(holder.getDomNode("//*:RegistryObjectList")).getXml()
        testCase.setPropertyValue("assertion", assertion)
        testCase.setPropertyValue("RegistryObjectList", registryObjectList)
        def testCaseContext = new com.eviware.soapui.support.types.StringToObjectMap();
        testCase.run(testCaseContext, false)

        // retrieve iti-57 simu response
        ////////////////////////////////
        def registryResponse = testCase.getTestStepByName("ITI-57").getPropertyValue("response")
        if (registryResponse.contains("Success")) {
            context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
            return "Success_response"
        } else {
            errorCode = "XDSMetadataUpdateError"
            codeContext = "General metadata update error."
            context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />');
            return 'Error_response'
        }
    } else {
        errorCode = "XDSPatientIDReconciliationError"
        codeContext = "Update encountered an error where patient identifiers did not match"
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />');
        return "Error_response"
    }

} catch (Exception e) {
    errorCode = "XDSMetadataUpdateError"
    codeContext = "General metadata update error"
    context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('soapError', '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="' + codeContext + '" />');
    return 'Error_response'
}


boolean checkCommunity(sql, homeId, patientId) {
    def req2 = "select count(*) as nbrMess from document_registry where patient_id like ? and home_community_id like ?;"

    sql.eachRow(req2, [patientId, homeId]) { p -> nbrMess = "${p.nbrMess}" }
    nbrMess = Integer.parseInt(nbrMess)

    if (nbrMess < 1) {
        return false;
    }

    return true;
}


//// Check assertion SAML validaton
///////////////////////////
def assertionValidation(def assertion) {
    log.info "Assertion SAML :"
    def project = mockRequest.getContext().getMockService().getProject()
    def testCase = project.testSuites['Library'].testCases['assertionValidation TestCase']

    if (assertion == 'null') {
        errorCode = "SAMLAssertionError"
        messageError = '<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="' + errorCode + '" codeContext="Missing SAML Assertion" />'
        context.mockService.setPropertyValue('soapError', messageError)
        return false;
    }
    assertion = assertion.replace('<?xml version="1.0" encoding="UTF-8"?>', '')
    testCase.setPropertyValue('assertionDocument', assertion)
    def testCaseContext = new com.eviware.soapui.support.types.StringToObjectMap();
    def runner = testCase.run(testCaseContext, false)
    def getValidationAssertion = testCase.getPropertyValue("validationResponse")
    def getErrorMessage = testCase.getPropertyValue("errorMessage")

    if (!getValidationAssertion.contains('<ValidationTestResult>PASSED</ValidationTestResult>')) {
        context.mockService.setPropertyValue('soapError', getErrorMessage)
        return false;
    }
    return true;
}