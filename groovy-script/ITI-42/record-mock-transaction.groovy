
/// IMPORT
//////////////
import org.apache.commons.lang.StringUtils;
import com.eviware.soapui.support.XmlHolder
import net.ihe.gazelle.wstester.mockrecord.Message
import net.ihe.gazelle.wstester.mockrecord.MessageRecorder
import static net.ihe.gazelle.wstester.mockrecord.EStandard.*
import java.nio.charset.StandardCharsets

def holderRequest = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
def holderResponse = new com.eviware.soapui.support.XmlHolder(context.expand('${#MockService#response}'))


/////////////////////////////////////
/////////////////////////////////////
def simulatedActorKeyword_init = "DOCUMENT_REPOSITORY"
def simulatedActorKeyword_resp = "DOCUMENT_REGISTRY"
def domainKeyword = "EPR"
def transactionKeyword = "ITI-42"
def responder_ip = "documentRegistrySimulator"
def standard = XDS
/////////////////////////////////////
/////////////////////////////////////

//
def request = mockRequest.requestContent;
def response = mockResponse.responseContent;
def sender_ip = mockRequest.getHttpRequest().getRemoteAddr()
def responseRoot = new XmlSlurper().parseText(response)
def requestRoot = new XmlSlurper().parseText(request)
def responseType =  responseRoot.Body.'*'[0].name()
def requestType = requestRoot.Body.'*'[0].name()



/////// CAN BE CHANGED ACCORDING TO THE PROPERTIES USED IN THE RESPONSE
////////////////////////////////////////////////////////////////////////
int countProperties = StringUtils.countMatches(response.toString(), '${')
if (countProperties == 1) {
    def properties = response.substring(response.indexOf('${#MockService#'), response.indexOf('}') + 1).toString()
    def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()
    response = response.replace(properties, requestContext.getProperty(propertiesName))
} else {
    for (int i = 1; i <= countProperties; i++) {
        def properties = response.substring(response.indexOf('${#MockService#'), response.indexOf('}') + 1).toString()
        def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()

        if(requestContext.getProperty(propertiesName)==null){
            response = response.replace(properties, "")
        }else{
            response = response.replace(properties, requestContext.getProperty(propertiesName))
        }
    }
}

////////

byte[] byte_request = request.getBytes(StandardCharsets.UTF_8)
byte[] byte_response = response.getBytes(StandardCharsets.UTF_8)

MessageRecorder messageRecorder = new MessageRecorder("jdbc:postgresql://localhost:5432/gazelle-webservice-tester", "gazelle", "gazelle")
Message requestMessage = new Message(sender_ip, sender_ip, requestType, simulatedActorKeyword_init, byte_request)
Message responseMessage = new Message(responder_ip, responder_ip, responseType, simulatedActorKeyword_resp, byte_response)
messageRecorder.record(standard, transactionKeyword, domainKeyword, simulatedActorKeyword_resp, requestMessage, responseMessage)
