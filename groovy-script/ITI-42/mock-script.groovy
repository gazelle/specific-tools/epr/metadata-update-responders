import org.apache.commons.codec.binary.Base64;
import groovy.sql.Sql;
import javax.sql.rowset.serial.*

// create XmlHolder for request content
def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/update_metadata", "gazelle", "gazelle", "org.postgresql.Driver")

if(countPatientID(sql) >= 1){
    if(countDocumentPatient(sql) >= 1){

        mess = getDocumentInRegistry(sql)

        part3 = mess.indexOf('<lcm:SubmitObjectsRequest');
        part4 = mess.indexOf('<rim:RegistryObjectList>');
        submitObject = mess.substring(part3, part4+24)
        mess = mess.replace(submitObject, '')
        part1 = mess.indexOf('<rim:ExtrinsicObject');
        part2 = mess.indexOf('</rim:ExtrinsicObject>');
        extrinsicObject = mess.substring(part1, part2+23)

        context.mockService.setPropertyValue('document', extrinsicObject.trim())
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        return 'Success_Response'

    }else{
        errorCode = "XDSUnknownCommunity"
        codeContext = "A value for the homeCommunityId is not recognize"
        context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
        context.mockService.setPropertyValue('soapError','<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="'+errorCode+'" codeContext="'+codeContext+'" />')
        return 'Error_response'
    }
}else{
    errorCode = "XDSUnknownPatientId"
    codeContext = "Patient ID in Document (Document1) doesn't exist"
    context.mockService.setPropertyValue('uuid1', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', 'urn:uuid:'+UUID.randomUUID().toString())
    context.mockService.setPropertyValue('soapError','<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="'+errorCode+'" codeContext="'+codeContext+'" />')
    return 'Error_response'

}




////////////////////////////////////////////
///////// FUNCTIONS LIST
////////////////////////////////////////////

/// get HomeCommunity request
def getHomeCommunityId(){
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def idHome = holder['//*:AdhocQuery/@home']
    return idHome;
}


/// get PatientID request
def getPatientId(){
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def idPatient_req = holder['//*:Slot[@name="$XDSDocumentEntryPatientId"]/*:ValueList/*:Value']
    idPatient_req = idPatient_req.replace("'", "");

    return idPatient_req;
}

/// get IdSource request
def getIdSource(){
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def idSource_req = holder['//*:Slot[@name="$XDSSubmissionSetSourceId"]/*:ValueList/*:Value']
    return idSource_req ;
}

/// get ValidityStatus request
def getValidityStatus(){
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
    def validity = holder['//*:Slot[@name="$XDSDocumentEntryStatus"]/*:ValueList/*:Value']
    partVald = validity.indexOf('StatusType:');
    vald = validity.substring(0, partVald +11)
    validity = validity.replace(vald, '')
    validity = validity.replace("')", '')
    return validity ;
}

/// Count Document for one patient in DB
int countDocumentPatient(def sql){
    def req2 = "select count(*) as nbrMess from document_registry where availability_status like ? and patient_id like ? and home_community_id like ?;"
    def validity = getValidityStatus();
    String idPatient_req = getPatientId();
    String home = getHomeCommunityId();

    sql.eachRow(req2, [validity, idPatient_req, home]) { p -> nbrMess =  "${p.nbrMess}"}
    nbrMess = Integer.parseInt(nbrMess)

    return nbrMess;
}


/// Count Document for one patient in DB
int countPatientID(def sql){
    def req2 = "select count(*) as nbrMess from document_registry where patient_id like ?;"
    String idPatient_req = getPatientId()

    sql.eachRow(req2, [idPatient_req]) { p -> nbrMess =  "${p.nbrMess}"}
    nbrMess = Integer.parseInt(nbrMess)

    return nbrMess;
}

/// get Document in the database
def getDocumentInRegistry(def sql){
    def req = "select document from document_registry where availability_status like ? and patient_id like ?;"
    def validity = getValidityStatus();
    def idPatient_req = getPatientId();

    request = sql.eachRow(req, [validity, idPatient_req])
            { p -> message =  "${p.document}"}
    mess = new String(message.decodeBase64())

    return mess;
}
