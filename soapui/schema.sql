CREATE TABLE "document_registry"(
 id SERIAL PRIMARY KEY,
 patientId_doc VARCHAR(100) NOT NULL,
 uniqueId_doc VARCHAR(100) NOT NULL,
 sourceId_doc VARCHAR(100) NOT NULL,
 version_doc INT NOT NULL,
 lastVersion_doc VARCHAR(100) NOT NULL,
 publishBy VARCHAR(10) NOT NULL,
 resource_id VARCHAR(100) NOT NULL,
 document_doc VARCHAR(300000) NOT NULL
);