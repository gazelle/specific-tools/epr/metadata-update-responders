#! /bin/sh
### BEGIN INIT INFO
# Provides:          rmuRespondingGatewayMock
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: SoapUI mock-up for EPR RMU Responding Gateway
# Description:       SoapUI mock-up for EPR RMU Responding Gateway
### END INIT INFO

#
# Author: Cedric Eoche-Duval <cel@kereval.com>
#

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

SOAPUI_PATH=/usr/local/SmartBear/SoapUI-5.3.0
SOAPUI_PROJECT_PATH=/opt/simulators/metadata-update-responders/soapui/RMU-soapui-project.xml
SOAPUI_MOCK_NAME=RespondingGateway_Binding_Soap12_MockService
SOAPUI_MOCK_PORT=8095
SOAPUI_MOCK_ENDPOINT=/restricted-metadata-update
SOAPUI_MOCK_LOG=/var/log/soapui/epr-restricted-metadata-update.log


case "$1" in
  start)
    echo "Starting $SOAPUI_MOCK_NAME ..."
    $SOAPUI_PATH/bin/mockservicerunner.sh -m "$SOAPUI_MOCK_NAME" -p "$SOAPUI_MOCK_PORT" -a "$SOAPUI_MOCK_ENDPOINT" $SOAPUI_PROJECT_PATH >> $SOAPUI_MOCK_LOG 2>&1 &
    if [ "$?" = '0' ]; then
	echo "Mock '$SOAPUI_MOCK_NAME' is started"
	exit 0
    else
        echo "Mock '$SOAPUI_MOCK_NAME' failed to start"
        exit 1
    fi
    ;;
  stop)
    echo "Stopping $SOAPUI_MOCK_NAME ..."
    ps aux| grep -i mockservicerunner.sh | grep $SOAPUI_MOCK_NAME | awk {'print $2'} | xargs kill -9
    netstat -ap | grep :$SOAPUI_MOCK_PORT | awk {'print $7'} | cut -d "/" -f1 | xargs kill -9
    if [ "$?" = '0' ]; then
        echo "Mock '$SOAPUI_MOCK_NAME' is stopped"
        exit 0
    else
        echo "Mock '$SOAPUI_MOCK_NAME' failed to stop"
        exit 1
    fi
    ;;
  status)
    pid=$(ps aux| grep -i mockservicerunner.sh | grep $SOAPUI_MOCK_NAME | awk {'print $2'})
    if [ -z "$pid" ]; then
      echo "Service $SOAPUI_MOCK_NAME is not running"
    else
      echo "Service $SOAPUI_MOCK_NAME is running [pid: $pid] [port: $SOAPUI_MOCK_PORT] [path: $SOAPUI_MOCK_PATH]"
    fi
    exit 0
    ;;
  *)
    echo "Usage: /etc/init.d/$NAME {start|stop|status}"
    exit 1
    ;;
esac

exit 0
